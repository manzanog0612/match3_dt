using System;
using System.Collections.Generic;
using UnityEngine;

using Match3.Gameplay.Management;

namespace Match3.Gameplay.Animation
{
    public enum ENTRY_TYPE { UP, DOWN, LEFT, RIGHT }   

    public class AnimationManager : MonoBehaviour
    {
        List<GameObject> jellies = null;

        const int amountEntries = 4;
        const int animationTypes = 3;

        public Action<AnimationComponent> OnLastAnimationCreated;
        public Action OnStartAnimationSecuence;
        public Action<AnimationComponent> OnAnimationComponentSet;

        int[] generalAnimationsRunning = new int[animationTypes];
        public bool entryAnimationDone = false;

        void Awake()
        {
            for (int i = 0; i < generalAnimationsRunning.Length; i++)
            {
                generalAnimationsRunning[i] = 0;
            }
        }

        public void SetJelliesGoList(List<GameObject> _jellies)
        {
            jellies = _jellies;
        }

        public void AddGeneralAnimationsRunning(ANIMATION_TYPE animationType)
        {
            generalAnimationsRunning[(int)animationType]++;
        }

        public void SubtractGeneralAnimationsRunning(ANIMATION_TYPE animationType)
        {
            generalAnimationsRunning[(int)animationType]--;
        }

        public bool CheckAnyPreviousAnimationRunning(ANIMATION_TYPE animationType)
        {
            ANIMATION_TYPE previousAnimationType = (int)animationType == 0 ? (ANIMATION_TYPE)(animationTypes - 1) : (ANIMATION_TYPE)((int)animationType - 1);

            return generalAnimationsRunning[(int)previousAnimationType] > 0;
        }

        public void SetActionsForAnimationComponent(AnimationComponent component)
        {
            OnAnimationComponentSet?.Invoke(component);
            component.OnGeneralAnimationStarted += AddGeneralAnimationsRunning;
            component.OnGeneralAnimationEnded += SubtractGeneralAnimationsRunning;       
        }

        public void SetActionsForAnimationComponentForNewJelly(AnimationComponent component)
        {
            SetActionsForAnimationComponent(component);
            component.generalAnimationsEnabled = true;
            component.SetSpawnAnimation();
        }

        public void StartGridCreationAnimation()
        {
            OnStartAnimationSecuence?.Invoke();

            ENTRY_TYPE entry = (ENTRY_TYPE)UnityEngine.Random.Range(0, amountEntries);

            int sortInLayer = 1;
            for (int i = 0; i < jellies.Count; i++)
            {
                AnimationComponent actual = jellies[i].GetComponent<AnimationComponent>();
                AnimationComponent next = null;
                SpriteRenderer spriteRenderer = jellies[i].gameObject.GetComponent<SpriteRenderer>();

                if (i < jellies.Count - 1)
                {
                    next = jellies[i + 1].GetComponent<AnimationComponent>();

                    actual.OnMakingAnimation += next.StartEntryAnimation;                    
                }
                else
                {
                    OnLastAnimationCreated?.Invoke(actual);
                }

                actual.entry = entry;

                spriteRenderer.color = new Color(0,0,0,0);
                spriteRenderer.sortingOrder = sortInLayer;
                sortInLayer++;

                SetActionsForAnimationComponent(actual);
            }

            jellies[0].GetComponent<AnimationComponent>().StartEntryAnimation();
        }
    }
}


