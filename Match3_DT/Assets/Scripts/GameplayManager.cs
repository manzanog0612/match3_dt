using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Match3.Gameplay.Entities.Grid;
using Match3.Gameplay.Entities.Grid.Jellies;
using Match3.Gameplay.User;
using Match3.Gameplay.UI;
using Match3.Gameplay.Animation;
using Match3.Gameplay.Audio;

namespace Match3.Gameplay.Management
{
    public class GameplayManager : MonoBehaviour
    {
        private static GameplayManager instance = null;

        public static GameplayManager GetInstance()
        {
            return instance;
        }

        void Awake()
        {
            instance = this;

            animationManager = GetComponent<AnimationManager>();
            grid.OnInstantiateJelly += IntantiateJelly;
            initialTurns = turns;
        }

        [SerializeField] JellyGrid grid = null;
        [SerializeField] UserInput userInput = null;
        [SerializeField] UIManager uIManager = null;
        [SerializeField] SoundManager soundManager = null;

        AnimationManager animationManager = null;

        [SerializeField] GameObject jellyPrefab = null;
        [SerializeField] Sprite[] jellySprites = null;
        [SerializeField] int turns = 10;

        int initialTurns = 0;
        int score = 0;
        bool gameplayActive = false;

        List<Tile> tiles = null;

        List<Jelly> selectedJellies = new List<Jelly>();

        public void RestartGame()
        {
            animationManager.entryAnimationDone = false;
            gameplayActive = false;

            grid.RestartGrid();

            tiles = grid.GetTiles();

            score = 0;
            turns = initialTurns;

            uIManager.UpdateTurnsLeft(turns);
            uIManager.UpdateScore(score);

            animationManager.SetJelliesGoList(grid.GetJelliesGOs());
            animationManager.StartGridCreationAnimation();
        }

        public void SetEntryAnimationDone()
        {
            animationManager.entryAnimationDone = true;
        }

        public void SetLastAnimationActions(AnimationComponent actual)
        {
            actual.OnEntryAnimationEnded += SetGameplayActive;
            actual.OnEntryAnimationEnded += SetEntryAnimationDone;
            actual.OnEntryAnimationEnded += EnableInput;            
        }

        public void SetInputStateAtAnimationState(AnimationComponent animation)
        {
            animation.OnGeneralAnimationStarted += DisableInput;
            animation.OnGeneralAnimationEnded += EnableInput;            
        }

        public void SetSoundAtEndOfEntryAnimation(AnimationComponent animation)
        {
            animation.OnEntryAnimationEnded += soundManager.PlayBubble;
        }

        public void SetGameplayActive()
        {
            gameplayActive = true;
        }

        void AddScore()
        {
            if (!gameplayActive) return;

            score += 5;

            uIManager.UpdateScore(score);
        }

        void SubtractTurn()
        {
            turns--;

            if (turns == 0)
            {
                uIManager.SetRestartPanelActive();

                RestartGame();
            }
            else
            {
                uIManager.UpdateTurnsLeft(turns);
            }
        }

        bool CheckSameTypeAndAdjacent(Jelly jelly1, Jelly jelly2)
        {
            Tile jelly1Tile = grid.GetTileOfJelly(jelly1);
            Tile jelly2Tile = grid.GetTileOfJelly(jelly2);

            bool adjacent = Mathf.Abs(jelly2Tile.column - jelly1Tile.column) <= 1 &&
                            Mathf.Abs(jelly2Tile.row - jelly1Tile.row) <= 1;

            return jelly1.Type == jelly2.Type && adjacent;
        }

        bool CheckIfJellyGoodForSelection(Jelly jelly)
        {
            return selectedJellies.Count == 0 ||
                (!selectedJellies.Contains(jelly) &&
                CheckSameTypeAndAdjacent(jelly, selectedJellies.Last()));
        }

        void AddSelectedJelliesToList(RaycastHit2D hit)
        {
            Jelly jelly = hit.transform.GetComponent<Jelly>();

            if (jelly == null)
            {
                selectedJellies.Clear();
                return;
            }

            if (CheckIfJellyGoodForSelection(jelly))
            {
                selectedJellies.Add(jelly);
                soundManager.PlayFallingBeep();
                jelly.State = Jelly.STATE.SELECTED;
            }
            else if (selectedJellies.Count >= 2 && selectedJellies.Contains(jelly) &&
                     selectedJellies[selectedJellies.Count - 2] == jelly)
            {
                selectedJellies.Last().State = Jelly.STATE.NORMAL;
                selectedJellies.Remove(selectedJellies.Last());
            }
        }

        void ClearSelectedJelliesList()
        {
            if (selectedJellies.Count >= grid.GetJelliesMinCombination())
            {
                for (int i = 0; i < selectedJellies.Count; i++)
                {
                    Jelly jelly = selectedJellies[i];

                    jelly.EliminateJelly();
                    grid.GetTileOfJelly(jelly).jelly = null;
                }
                
                SubtractTurn();
                soundManager.PlayGood();
            }
            else
            {
                for (int i = 0; i < selectedJellies.Count; i++)
                {
                    Jelly jelly = selectedJellies[i];

                    jelly.State = Jelly.STATE.NORMAL;
                }
                
                soundManager.PlayError();

            }

            selectedJellies.Clear();
        }

        Jelly IntantiateJelly(JELLY_TYPE type, Vector2 position)
        {
            GameObject jellyGO = Instantiate(jellyPrefab, grid.transform);

            jellyGO.transform.position = position;
            jellyGO.GetComponent<SpriteRenderer>().sprite = jellySprites[(int)type];

            Jelly jelly = jellyGO.GetComponent<Jelly>();
            AnimationComponent animationComponent = jellyGO.GetComponent<AnimationComponent>();

            jelly.Type = type;
            jelly.OnDeath += AddScore;
            jelly.OnDeath += animationComponent.SetDieAnimation;
            jelly.OnFall += animationComponent.SetFallAnimation;

            if (animationManager.entryAnimationDone)
            {
                animationManager.SetActionsForAnimationComponentForNewJelly(animationComponent);
            }

            return jelly;
        }

        void UpdateGrid()
        {
            if (!gameplayActive) return;
            grid.SetLoopOfGridWithAnimation();
        }

        void DisableInput()
        {
            userInput.allowed = false;
        }

        void EnableInput()
        {
            if (!animationManager.entryAnimationDone) return;
                userInput.allowed = true;
        }

        void DisableInput(ANIMATION_TYPE anyAnimation)
        {
            userInput.allowed = false;
        }

        void EnableInput(ANIMATION_TYPE anyAnimation)
        {
            if (!animationManager.entryAnimationDone) return;
                userInput.allowed = true;
        }

        void Start()
        {
            tiles = grid.GetTiles();

            userInput.OnLeftButtonDown += AddSelectedJelliesToList;
            userInput.OnLeftButtonUp += ClearSelectedJelliesList;
            userInput.OnLeftButtonUp += UpdateGrid;

            uIManager.UpdateTurnsLeft(turns);
            uIManager.UpdateScore(score);

            animationManager.OnLastAnimationCreated += SetLastAnimationActions;
            animationManager.OnStartAnimationSecuence += DisableInput;
            animationManager.OnAnimationComponentSet += SetInputStateAtAnimationState;
            animationManager.OnAnimationComponentSet += SetSoundAtEndOfEntryAnimation;

            animationManager.SetJelliesGoList(grid.GetJelliesGOs());
            animationManager.StartGridCreationAnimation();
        }

        void OnDestroy()
        {
            userInput.OnLeftButtonDown -= AddSelectedJelliesToList;
            userInput.OnLeftButtonUp -= ClearSelectedJelliesList;
            userInput.OnLeftButtonUp -= UpdateGrid;

            grid.OnInstantiateJelly -= IntantiateJelly;

            animationManager.OnLastAnimationCreated -= SetLastAnimationActions;
            animationManager.OnStartAnimationSecuence -= DisableInput;
            animationManager.OnAnimationComponentSet -= SetInputStateAtAnimationState;
            animationManager.OnAnimationComponentSet -= SetSoundAtEndOfEntryAnimation;
        }
    }
}

