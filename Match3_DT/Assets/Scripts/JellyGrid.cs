using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Match3.Gameplay.Entities.Grid.Jellies;
using Match3.Gameplay.Animation;

namespace Match3.Gameplay.Entities.Grid
{
    public enum JELLY_TYPE { EARTH, DEMON, FOREST, FIRE, WATER, ARTHROPODA}

    [System.Serializable]
    public class Tile
    {
        public Tile(Jelly _jelly, Vector2 _position, int _row, int _column)
        {
            jelly = _jelly;
            position = Vector2.zero;
            row = 0;
            column = 0;
        }

        public Tile()
        {
            jelly = null;
            position = Vector2.zero;
            row = 0;
            column = 0;
        }

        public Jelly jelly = null;
        public Vector2 position = Vector2.zero;
        public int row = 0;
        public int column = 0;
    }

    public class JellyGrid : MonoBehaviour
    {
        public const int amountJellyTypes = 6;

        [SerializeField] int columns = 10;
        [SerializeField] int rows = 6;
        [SerializeField] int jelliesMinCombination = 3;
        [SerializeField] AnimationManager animationManager;

        [SerializeField] JellyToInstantiate[] jelliesToSpawn = new JellyToInstantiate[amountJellyTypes];

        [SerializeField] List<Tile> tiles = null;

        public delegate Jelly OnInstantiateJellyDelegate(JELLY_TYPE type, Vector2 position);
        public OnInstantiateJellyDelegate OnInstantiateJelly = null;      

        public List<Tile> GetTiles()
        {
            return tiles;
        }

        public Tile GetTileOfJelly(Jelly jelly)
        {
            for (int i = 0; i < tiles.Count; i++)
            {
                if (jelly == tiles[i].jelly)
                {
                    return tiles[i];
                }
            }

            return null;
        }

        public List<GameObject> GetJelliesGOs()
        {
            List<GameObject> jelliesGOs = new List<GameObject>();

            foreach (Tile tile in tiles)
            {
                jelliesGOs.Add(tile.jelly.gameObject);
            }

            return jelliesGOs;
        }

        public int GetJelliesMinCombination()
        {
            return jelliesMinCombination;
        }

        void GenerateTiles()
        {
            float spaceBetweenJellies = 0.3f;
            int jellySize = 1;

            float gridHeight = rows * jellySize + rows * spaceBetweenJellies;
            float gridWidth = columns * jellySize + columns * spaceBetweenJellies;

            Vector2 initialPos = new Vector2(-gridWidth / 2f + jellySize / 2f + spaceBetweenJellies / 2f,
                                              gridHeight / 2f - jellySize / 2f - spaceBetweenJellies / 2f);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Tile tile = new Tile();
                    Vector2 pos = initialPos;

                    pos.x += j * jellySize + j * spaceBetweenJellies;
                    pos.y += i * -jellySize + i * -spaceBetweenJellies;

                    tile.column = j;
                    tile.row = i;
                    tile.position = pos;
                    tile.jelly = null;

                    tiles.Add(tile);
                }
            }
        }

        void AddNewJellyToTile(int index)
        {
            int id;

            do
            {
                id = UnityEngine.Random.Range(0, amountJellyTypes);

            } while (!jelliesToSpawn[id].spawn);

            tiles[index].jelly = OnInstantiateJelly?.Invoke((JELLY_TYPE)id, tiles[index].position);

        }

        void DeactivateIfMinCombinationReached(List<Tile> tilesOfJelliesToDeactivate)
        {
            if (tilesOfJelliesToDeactivate.Count >= jelliesMinCombination)
            {
                for (int i = 0; i < tilesOfJelliesToDeactivate.Count; i++)
                {
                    Tile tile = tilesOfJelliesToDeactivate[i];

                    tile.jelly.EliminateJelly();
                    tile.jelly = null;
                }
            }

            tilesOfJelliesToDeactivate.Clear();
        }

        void AddToListIfAdjacent(List<Tile> tilesOfJelliesToDeactivate, Tile actualTile, Tile previousTile)
        {
            Jelly actualJelly = actualTile.jelly;
            Jelly previousJelly = previousTile.jelly;

            if (actualJelly != null && previousJelly != null && actualJelly.Type == previousJelly.Type)
            {
                if (!tilesOfJelliesToDeactivate.Contains(previousTile))
                    tilesOfJelliesToDeactivate.Add(previousTile);

                tilesOfJelliesToDeactivate.Add(actualTile);
            }
            else
            {
                DeactivateIfMinCombinationReached(tilesOfJelliesToDeactivate);
            }
        }

        void EliminateSetCombinations()
        {
            List<Tile> tilesOfJelliesToDeactivate = new List<Tile>();
            Tile previousJellyTile = null;

            //horizontal check
            for (int r = 0; r < rows; r++)
            {
                previousJellyTile = tiles[r * columns];
                tilesOfJelliesToDeactivate.Clear();

                for (int c = 1; c < columns; c++)
                {
                    AddToListIfAdjacent(tilesOfJelliesToDeactivate, tiles[r * columns + c], previousJellyTile);

                    previousJellyTile = tiles[r * columns + c];
                }

                DeactivateIfMinCombinationReached(tilesOfJelliesToDeactivate);
            }

            //vertical check
            for (int c = 0; c < columns; c++)
            {
                previousJellyTile = tiles[c];
                tilesOfJelliesToDeactivate.Clear();

                for (int r = 1; r < rows; r++)
                {
                    if (previousJellyTile.jelly != null && tiles[r * columns + c].jelly != null)
                    {
                        AddToListIfAdjacent(tilesOfJelliesToDeactivate, tiles[r * columns + c], previousJellyTile);
                    }

                    previousJellyTile = tiles[r * columns + c];
                }

                DeactivateIfMinCombinationReached(tilesOfJelliesToDeactivate);
            }
        }

        bool CheckIfAnyJellyMissing(int column, out int row)
        {
            for (int r = rows - 1; r >= 0; r--)
            {
                if (tiles[r * columns + column].jelly == null)
                {
                    row = r;
                    return true;
                }
            }

            row = -1;
            return false;
        }

        void AcomodateActiveJellies()
        {
            List<int> deactivatedJelliesRows = new List<int>();

            for (int c = columns - 1; c >= 0; c--)
            {
                int rowMissingJelly;

                if (CheckIfAnyJellyMissing(c, out rowMissingJelly))
                {
                    int spacesToMove = 1;

                    //get initial gap of rows without jellies
                    for (int r = rowMissingJelly - 1; r >= 0; r--)
                    {
                        if (tiles[r * columns + c].jelly == null)
                        {
                            spacesToMove++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    //check for separate rows without jellies
                    for (int r = rowMissingJelly - spacesToMove; r >= 0; r--)
                    {
                        if (tiles[r * columns + c].jelly == null)
                        {
                            deactivatedJelliesRows.Add(r);
                        }
                    }

                    //acomodate active jellies to the bottom
                    for (int r = rowMissingJelly; r >= 0; r--)
                    {
                        if (deactivatedJelliesRows.Contains(r - 1) && r != rowMissingJelly)
                            spacesToMove++;

                        if (r - spacesToMove < 0)
                        {
                            tiles[r * columns + c].jelly = null;
                        }
                        else
                        {
                            if (tiles[(r - spacesToMove) * columns + c].jelly == null)
                            {
                                tiles[r * columns + c].jelly = null;
                            }
                            else
                            {
                                tiles[r * columns + c].jelly = tiles[(r - spacesToMove) * columns + c].jelly;
                                tiles[r * columns + c].jelly.gameObject.transform.position = tiles[r * columns + c].position;

                                tiles[r * columns + c].jelly.SetFall(tiles[(r - spacesToMove) * columns + c].position);
                            }                            
                        }
                    }
                }

                deactivatedJelliesRows.Clear();
            }
        }

        void GenerateJelliesInEmptyTiles()
        {
            for (int c = 0; c < columns; c++)
            {
                for (int r = rows - 1; r >= 0; r--)
                {
                    if (tiles[r * columns + c].jelly == null)
                        AddNewJellyToTile(r * columns + c);
                }
            }
        }

        bool CheckAnyTileEmpty()
        {
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].jelly == null)
                    return true;
            }

            return false;
        }

        IEnumerator LoopGrid()
        {
            EliminateSetCombinations();

            while (animationManager.CheckAnyPreviousAnimationRunning(ANIMATION_TYPE.DISAPEAR))
            {
                yield return new WaitForEndOfFrame();
            }

            do
            {        
                AcomodateActiveJellies();

                while (animationManager.CheckAnyPreviousAnimationRunning(ANIMATION_TYPE.FALL))
                {
                    yield return new WaitForEndOfFrame();
                }

                GenerateJelliesInEmptyTiles();

                while (animationManager.CheckAnyPreviousAnimationRunning(ANIMATION_TYPE.RESPAWN))
                {
                    yield return new WaitForEndOfFrame();
                }

                EliminateSetCombinations();

                while (animationManager.CheckAnyPreviousAnimationRunning(ANIMATION_TYPE.DISAPEAR))
                {
                    yield return new WaitForEndOfFrame();
                }

            } while (CheckAnyTileEmpty());

            yield return null;
        }

        public void SetLoopOfGridWithAnimation()
        {
            StartCoroutine(LoopGrid()); 
        }

        public void SetLoopOfGrid()
        {
            EliminateSetCombinations();

            do
            {
                AcomodateActiveJellies();

                GenerateJelliesInEmptyTiles();

                EliminateSetCombinations();

            } while (CheckAnyTileEmpty());
        }

        public void RestartGrid()
        {
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].jelly != null)
                { 
                    Destroy(tiles[i].jelly.gameObject);
                    tiles[i].jelly = null;
                }
            }

            for (int i = 0; i < tiles.Count; i++)
            {
                AddNewJellyToTile(i);
            }

            SetLoopOfGrid();
        }

        void Awake()
        {
            tiles = new List<Tile>();

            GenerateTiles();

            for (int i = 0; i < tiles.Count; i++)
            {
                AddNewJellyToTile(i);
            }
        }

        void Start()
        {
            SetLoopOfGrid();
        }
    }
}
