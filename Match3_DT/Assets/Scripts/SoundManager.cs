﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3.Gameplay.Audio
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] AudioSource fallingBeep = null;
        [SerializeField] AudioSource error = null;
        [SerializeField] AudioSource good = null;
        [SerializeField] AudioSource bubble = null;
        [SerializeField] AudioSource mainTrack = null;

        public void PlayError()
        {
            error.Play();
        }

        public void PlayBubble()
        {
            bubble.Play();
        }

        public void PlayGood()
        {
            good.Play();
        }

        public void PlayFallingBeep()
        {
            fallingBeep.Play();
        }

        public void SetMainTrackMute(bool mute)
        {
            mainTrack.mute = mute;
        }
    }
}
