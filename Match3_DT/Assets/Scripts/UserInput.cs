using System;
using UnityEngine;

namespace Match3.Gameplay.User
{
    public class UserInput : MonoBehaviour
    {
        [SerializeField] LayerMask selectableMask = 0;

        public Action<RaycastHit2D> OnLeftButtonDown = null;
        public Action OnLeftButtonUp = null;

        public bool allowed = false;

        void Update()
        {
            if (!allowed) return;

            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hitData = Physics2D.Raycast(new Vector2(worldPosition.x, worldPosition.y), Vector3.forward, 20, selectableMask);

            if (Input.GetMouseButton(0) && hitData)
            {
                OnLeftButtonDown?.Invoke(hitData);               
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnLeftButtonUp?.Invoke();
            }
        }        
    }
}
