using System;
using UnityEngine;

namespace Match3.Gameplay.Entities.Grid.Jellies
{
    [System.Serializable]
    public class JellyToInstantiate
    {
        public JELLY_TYPE type = JELLY_TYPE.EARTH;
        public bool spawn = true;
    }

    public class Jelly : MonoBehaviour
    {
        public enum STATE { SELECTED, NORMAL }

        private JELLY_TYPE type = JELLY_TYPE.EARTH;
        private STATE state = STATE.NORMAL;

        public JELLY_TYPE Type { get { return type; } set { type = value; } }

        public STATE State { 

            get { return state; } 
            set 
            { 
                state = value;
                switch (state)
                {
                    case STATE.SELECTED:
                        anim.SetBool("Grabbed", true);
                        break;
                    case STATE.NORMAL:
                        anim.SetBool("Grabbed", false);
                        break;
                    default:
                        break;
                }
            } 
        }

        public Action OnDeath = null;
        public Action<Vector2> OnFall = null;

        Animator anim = null;

        public void EliminateJelly()
        {
            if (anim != null)
                anim.enabled = false;
            OnDeath?.Invoke();
        }

        public void SetFall(Vector2 initialPosition)
        {
            OnFall?.Invoke(initialPosition);
        }

        void Awake()
        {
            anim = GetComponent<Animator>();
        }

        void OnDestroy()
        {            
            OnDeath = null;
        }
    }
}
