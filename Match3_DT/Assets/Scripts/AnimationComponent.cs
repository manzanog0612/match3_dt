using System;
using System.Collections;
using UnityEngine;

namespace Match3.Gameplay.Animation
{
    public enum ANIMATION_TYPE { DISAPEAR, FALL, RESPAWN}

    public class AnimationComponent : MonoBehaviour
    {
        public Action OnMakingAnimation = null;
        public Action OnEntryAnimationEnded = null;
        public Action<ANIMATION_TYPE> OnGeneralAnimationEnded = null;
        public Action<ANIMATION_TYPE> OnGeneralAnimationStarted = null;

        public ENTRY_TYPE entry = ENTRY_TYPE.UP;

        public bool generalAnimationsEnabled = false;

        IEnumerator Entry(Vector2 initialPos)
        {
            generalAnimationsEnabled = false;

            Vector2 finalPos = transform.position;
            float speed = 1f;
            float time = 0;
            float timeToNextAnimation = 0.2f;
            bool calledOnMakingAnimation = false;

            while (time < 1)
            {
                time += Time.deltaTime * speed;

                transform.position = Vector2.Lerp(initialPos, finalPos, time);

                if (time > timeToNextAnimation && !calledOnMakingAnimation)
                {
                    OnMakingAnimation?.Invoke();
                    calledOnMakingAnimation = true;
                }

                yield return new WaitForEndOfFrame();
            }

            transform.position = finalPos;

            OnEntryAnimationEnded?.Invoke();
            generalAnimationsEnabled = true;

            yield return null;
        }

        IEnumerator Disapear()
        {
            OnGeneralAnimationStarted?.Invoke(ANIMATION_TYPE.DISAPEAR);

            Vector3 initialScale = transform.localScale;
            float speed = 1f;
            float time = 0;

            while (time < 1)
            {
                time += Time.deltaTime * speed;

                transform.localScale = Vector3.Lerp(initialScale, Vector3.zero, time);
                yield return new WaitForEndOfFrame();
            }

            OnGeneralAnimationEnded?.Invoke(ANIMATION_TYPE.DISAPEAR);

            Destroy(gameObject);
        }

        IEnumerator Fall(Vector2 initialPos)
        {
            OnGeneralAnimationStarted?.Invoke(ANIMATION_TYPE.FALL);            

            Vector2 finalPos = transform.position;
            transform.position = initialPos;
            float speed = 1f;
            float time = 0;

            while (time < 1)
            {
                time += Time.deltaTime * speed;

                transform.position = Vector2.Lerp(initialPos, finalPos, time);

                yield return new WaitForEndOfFrame();
            }

            transform.position = finalPos;

            OnGeneralAnimationEnded?.Invoke(ANIMATION_TYPE.FALL);
        }

        IEnumerator Respawn()
        {
            OnGeneralAnimationStarted?.Invoke(ANIMATION_TYPE.RESPAWN);

            GetComponent<Animator>().enabled = false;

            Vector3 finalScale = transform.localScale;
            transform.localScale = Vector3.zero;
            float speed = 1f;
            float time = 0;

            while (time < 1)
            {
                time += Time.deltaTime * speed;

                transform.localScale = Vector3.Lerp(Vector3.zero, finalScale, time);
                yield return new WaitForEndOfFrame();
            }

            transform.localScale = finalScale;

            GetComponent<Animator>().enabled = true;

            OnGeneralAnimationEnded?.Invoke(ANIMATION_TYPE.RESPAWN);
        }

        public void SetSpawnAnimation()
        {
            if (!generalAnimationsEnabled) return;
            
            StartCoroutine(Respawn());
        }

        public void SetDieAnimation()
        {
            if (!generalAnimationsEnabled)
            { 
                Destroy(gameObject); 
            }
            else
            {
                StartCoroutine(Disapear());
            }
        }

        public void SetFallAnimation(Vector2 initialPos)
        {
            if (!generalAnimationsEnabled) return;

            StartCoroutine(Fall(initialPos));
        }

        public void StartEntryAnimation()
        {
            Vector2 initialPos = Vector2.zero;

            switch (entry)
            {
                case ENTRY_TYPE.UP:
                    initialPos = new Vector2(transform.position.x, 6);
                    break;
                case ENTRY_TYPE.DOWN:
                    initialPos = new Vector2(transform.position.x, -6);
                    break;
                case ENTRY_TYPE.LEFT:
                    initialPos = new Vector2(-10, transform.position.y);
                    break;
                case ENTRY_TYPE.RIGHT:
                    initialPos = new Vector2(10, transform.position.y);
                    break;
                default:
                    break;
            }

            gameObject.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);

            StartCoroutine(Entry(initialPos));
        }
    }
}
