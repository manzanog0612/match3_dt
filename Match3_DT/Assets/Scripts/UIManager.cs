using UnityEngine;
using TMPro;

namespace Match3.Gameplay.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI score = null;
        [SerializeField] TextMeshProUGUI turnsLeft = null;
        [SerializeField] GameObject restartPanel = null;

        public void UpdateScore(int amountScore)
        {
            score.text = "Score: " + amountScore;
        }

        public void UpdateTurnsLeft(int turns)
        {
            turnsLeft.text = "Turns Left: " + turns;
        }

        public void SetRestartPanelActive()
        {
            restartPanel.SetActive(true);
        }

        public void SetRestartPanelUnactive()
        {
            restartPanel.SetActive(false);
        }
    }
}

